var express = require('express');
var router = express.Router();
var fs = require("fs");
//const MongoClient = require('mongodb').MongoClient;
var formidable = require("formidable");

//const uri = "mongodb+srv://<kabo>:<inventory17!>@cluster0.bc7dh.mongodb.net/test?retryWrites=true&w=majority";
// const client = new MongoClient(uri, { useNewUrlParser: true, useUnifiedTopology: true });
// client.connect(err => {
//   const collection = client.db("test").collection("devices");
//   // perform actions on the collection object
//   client.close();
// });

/* GET home page. */
router.get('/', function(req, res, next) {
  // MongoClient.connect(uri, {useUnifiedTopology:true, useNewUrlParser:true}, (error,client) => {

  //   if(error){
  //     console.log(error);
  //   }

  //   client.db(dbName).collection("people").find({"category":"food"}).toArray((err,outcome)=>{

  //     if(err){
  //       console.log("the error is");
  //       console.log(err);
  //     }
      
  //     res.render('index', { title: 'Express' });
  //   });

  // });

  var data= {};
  data = fs.readFileSync("public/content/content.json");
  var dipotso = JSON.parse(data);
  console.log(dipotso);

  res.render('index', { title: 'Express', questions:dipotso });
  
});

router.get("/content/:QCode",(req,res)=>{
  var QCodeWNum = req.params.QCode;

  dipotso = fs.readFileSync("public/content/content.json");
  var data = JSON.parse(dipotso);
  var obj = {};
  data.forEach((question) => {
    console.log(question);
    if(question.codeWNum == QCodeWNum){
      obj = question;
      console.log("I said the object is");
      console.log(obj);
      
    }
  });

  var solPages = [];
  for(var i=1; i<= obj["solPages"]; i++){
    var picString = "";
    console.log("the code with num is " + QCodeWNum);
    if(i == 1){
      console.log("the point 1");
      picString = QCodeWNum + "A.jpg";
      console.log("the point 2");
    }else{
      picString = QCodeWNum + "A" + String(i) + ".jpg";
    }
    solPages.push({"image":picString});

  }

  console.log("the solution pages are ");
  console.log(solPages);

  res.render("questionPage", {"QCodeWNum":QCodeWNum, "state":obj["state"], "solPages":solPages});
})

router.post("/search", function(req,res){
  var form = new formidable.IncomingForm();

	form.parse(req, function(err, fields, files){
    if(err){
      console.log("the error is");
      console.log(err);
    }

    console.log("the search parameter is  ");
    console.log(fields.searchParameter);

    dipotso = fs.readFileSync("public/content/content.json");
    var data = JSON.parse(dipotso);
    var obj = [];

    if(fields.searchParameter == "Course"){

      data.forEach((question)=>{
        if(question["course"] == fields.text){
          obj.push(question);
          console.log("these are the results");
          console.log(obj);
        }
      });
    }else if(fields.searchParameter == "Question Text"){
      console.log("the second if statement");
      data.forEach((question)=>{
        if(question["text"].includes(fields.text)){
          obj.push(question);
          console.log("now these are the results");
          console.log(obj);
        }
      });
    }

    res.render('index', { title: 'Express', questions:obj });
  })
})

router.get("/postPage", (req,res)=>{
  res.render("postPage");
});

router.post("/postReceiver", (req,res)=>{
  res.render("loadStatus");
})

router.get("/admin",(req,res)=>{
  var data= {};
  data = fs.readFileSync("public/content/content.json");
  var dipotso = JSON.parse(data);
  console.log(dipotso);
  
  res.render("admin",{ questions:dipotso });
})

router.get("/adminQView/:QCode",(req,res)=>{
  var QCodeWNum = req.params.QCode;
  console.log("the QcodeWNum is");
  console.log(QCodeWNum);

  dipotso = fs.readFileSync("public/content/content.json");
  var data = JSON.parse(dipotso);
  var obj = {};
  data.forEach((question) => {
    console.log(question);

    if(question.codeWNum == QCodeWNum){
      obj = question;
      console.log("the object is");
      console.log(obj);     
    }
  });

  res.render("adminQuestionPage", {"QCodeWNum":QCodeWNum});
})

module.exports = router;
